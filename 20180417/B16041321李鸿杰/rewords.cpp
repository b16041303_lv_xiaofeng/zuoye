#include<iostream>
#include<cstring>
using namespace std;
void reverse(char * st,char * end)
{
	while(st<end)
	{
		char tmp = *st;
		*st = *end;
		*end = tmp;
		st++;end--;
	}
}

bool split(char c)
{
	if(c==' '||c==','||c=='.'||c=='!'||c=='?') return 1;
	else return 0;
}
void rewords(char *s)
{
	char *flag = s;
	char *st,*end;
	while(*flag)
	{
		while(split(*flag)&&(*flag)) flag++;
		st = flag;
		while(!split(*flag)&&(*flag)) flag++;
		end = flag-1;
		reverse(st,end);
	}
}
int main(){
	cout<<"Input:";
	char s[100];
	fgets(s,100,stdin);
	rewords(s);
	cout<<"Output:"<<s;
	return 0;
}