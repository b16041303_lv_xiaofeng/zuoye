#include<iostream>
using namespace std;

int isBD(char c)
{
	if(c==',') 
		return 1;
	if(c==' ') 
		return 1;
	if (c == '.')
		return 1;
	if(c=='?')	
		return 1;
	if(c=='!')	
		return 1;
	return 0;
} 

int main(){
	int i=0;
	char s[100];
	cin.get(s,100);
	while(s[i]!='\0')
	{
		while(isBD(s[i])==1&&s[i]!=0)   //为标点时输出 
		{
			cout<<s[i];					
			i++;
		}
		int start = i;                  //前一标点结束为下一单词首字母位置 
		while(isBD(s[i])==0&&s[i]!=0)
		{
			i++;
		}
		int end = i-1;                  //下一标点前为本单词最后一个字母位置 
		
		int m=start,n=end;
		while(m<n)                    	//将本单词逆置
		{					
			char t = s[m];
			s[m]=s[n];
			s[n]=t;
			m++;
			n--;
		}
		for(int j=start;j<=end;j++)         //输出 
		{		
			cout<<s[j];
		}
	}
	return 0;
} 
