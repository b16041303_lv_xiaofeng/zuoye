#include <iostream>
#include <iomanip>
using namespace std;
#define N 4
//二维数组a[n][n],元素旋转后有a[x][y]变成a[y][n-1-x],
void display(int x[][N]){
	for(int i=0;i<4;i++){
		for(int j=0;j<4;j++){
		cout<<setw(5)<<x[i][j];
		//	cout<<x[i][j]<<" ";
		}
		cout<<endl;
	}
}

int main(){
	int m,n;			//存放数组维度
	int a[4][4]={{1,2,3,4},{9,10,11,12},{13,9,5,1},{15,11,7,3}};
	int b[4][4]={0},c[4][4]={0};				//b存放顺时针旋转后的，c存放逆时针旋转后的 
	int i,j;
	for(i=0;i<4;i++){
		for(j=0;j<4;j++){
			b[j][3-i]=a[i][j];
			c[3-j][i]=a[i][j];
		}
	}
	
	cout<<"旋转前二维数组为："<<endl; 
	display(a);
	
	cout<<"顺旋转后二维数组为："<<endl; 
	display(b);
	
	cout<<"逆旋转后二维数组为："<<endl; 
	display(c);

	return 0;
} 
