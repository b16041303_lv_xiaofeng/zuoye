#include<stdio.h>

void ShowMat(int mat[4][4])
{
	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 4; j++)
			printf("\t%2d", mat[i][j]);
		printf("\n");
	}
}


int main(void)
{
	int mat[4][4] = { {1, 2, 3, 4}, {9, 10, 11, 12}, {13, 9, 5, 1}, {15, 11, 7, 3} };
	int newMat[4][4];
	ShowMat(mat);
	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			newMat[j][3-i] = mat[i][j];
		}
	}
	ShowMat(newMat);
	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			newMat[i][j] = mat[j][3-i];
		}
	}
	ShowMat(newMat);
	return 0;
}
