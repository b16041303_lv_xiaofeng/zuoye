#include<iostream>
#include<stdio.h>
using namespace std;
void printImage(int image[4][4])
{
    for(int i=0; i<4; ++i)
    {
        for(int j=0; j<4; ++j)
        {
            printf("%2d ",image[i][j]);
        }
        cout<<endl;
    }
}

void Clockwise90(int matrix[4][4])
{
    int b[4][4];
    for(int i=0;i<4;i++)
        for(int j=0;j<4;j++)
        b[j][4-1-i]=matrix[i][j];
        printImage(b);

}

void Counterclockwise90(int matrix[4][4])
{
    int b[4][4];
    for(int i=0;i<4;i++)
        for(int j=0;j<4;j++)
        b[3-j][i]=matrix[i][j];
        printImage(b);
}


int main(){
    int a[4][4]={{1,2,3,4},{9,10,11,12},{13,9,5,1},{15,11,7,3}};
    printImage(a);
    cout<<endl;
    Clockwise90(a);
    cout<<endl;
    Counterclockwise90(a);
    return 0;
}
