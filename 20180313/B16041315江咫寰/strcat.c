#include<stdio.h>

char* strcat(char* str1, char* str2)
{
    while(*str1!='\0')      //判断str1是否为空，直到读到'\0'结束
        str1++;
    while(*str2 != '\0')    //将str2字符串的内容续接到str1字符串的后面并覆盖str1的'\0'
        *str1++=*str2++;
    *str1='\0';
    return str1;
}

int main()
{
    char a[20],b[20];       //根据需求定义字符数组的长度
    printf("please input two strings:\n");
    scanf("%s",a);          //输入两个字符串
    scanf("%s",b);
    strcat(a,b);            //调用strcat函数合并两个字符串
    printf("\nreslut: %s\n",a);     //输出合并后的字符串
    return 0;
}
