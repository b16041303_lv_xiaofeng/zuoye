#include<stdio.h>

int Sumfor(int x)
{
	int sum=0,i=x;
	for(;i;i/=10)
	   sum+=i%10;
	return sum;
}
int Sumwhile(int x)
{
	int sum=0,i=x;
	while(i)
	{
		sum+=i%10;
		i/=10;
	}
	return sum;
}
int Sumdo(int x)
{
	int sum=0,i=x;
	do
	{
		sum+=i%10;
		i/=10;
	}while(i);
	return sum;
}
int Sumwhile1(int x)
{
	int sum=0,i=x;
	while(1)
	{
		sum+=i%10;
		i/=10;
		if(!i)
		  break;
	}
	return sum;
}
int main()
{
	int x;
	scanf("%d",&x);
	printf("Sumfor:%d\n",Sumfor(x));
	printf("Sumwhile:%d\n",Sumwhile(x));
	printf("Sumdo:%d\n",Sumdo(x));
	printf("Sumwhile1:%d\n",Sumwhile1(x));
	return 0;
}
