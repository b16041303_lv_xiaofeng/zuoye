#include <iostream>

using namespace std;




int num_while(int n)
{
    int sum=0;
    sum=sum+n%10;
    while(n/10!=0)
    {
        n=n/10;
        sum=sum+n%10;
    }
    return sum;
}
int num_dowhile(int n)
{
    int sum=0;
    do
    {
        sum=sum+n%10;
        n=n/10;
    }while(n);
    return sum;
}
int num_while1(int n)
{
    int sum=0;
    while(1)
    {
        sum=sum+n%10;
        n=n/10;
        if(n==0)break;
    }
    return sum;
}
int num_for(int n)
{
    int sum=0;
    for(sum+=n%10;n/10;)
    {
        n=n/10;
        sum+=n%10;
    }
    return sum;
}
int main()
{
    int x;
    cin >> x;
    cout << num_for(x) << " "<< num_while(x)<< " " <<num_dowhile(x)<< " " <<num_while1(x)<< endl;

    return 0;
}
