#include<stdio.h>
int f1(int x)
{
	int sum=0,i;
	for( ;x>0;x=x/10)
	{
		i=x%10;
		sum+=i;
	}
	printf("%d\n",sum);
	return 0;
} 
int f2(int x)
{
	int sum=0,i;
	while(x>0)
	{
		i=x%10;
		sum+=i;
		x=x/10;
	}
	printf("%d\n",sum);
	return 0;
}
int f3(int x)
{
	int sum=0,i;
	do{
		i=x%10;
		sum+=i;
		x=x/10;
	}while(x>0);
	printf("%d\n",sum);
	return 0;
}
int f4(int x)
{
	int sum=0,i;
	while(1)
	{
		i=x%10;
		sum+=i;
		x=x/10;
		if(x==0)
		break;
	}
	printf("%d\n",sum);
	return 0;
}

int main(){
	int x;
	scanf("%d",&x);
	f1(x);
	f2(x);
	f3(x);
	f4(x);
	return 0;
}