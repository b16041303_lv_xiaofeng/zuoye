#include<cstdio>
#include<iostream>
#include<cstring>
 
using namespace std;

char* Reverse(char s[]) //反转函数 
{
		// p指向字符串头部
    char* p = s ;

    // q指向字符串尾部
    char* q = s ;
    while( *q )
        ++q ;
    q -- ;

    // 交换并移动指针，直到p和q交叉
    while(q > p)
    {
        char t = *p ;
        *p++ = *q ;
        *q-- = t ;
    }

    return s ;
}

int Symbol(char s)
{
	if(s==' ' || s==',' || s=='.' || s=='!')
	return 1;
	else return 0;
}

void Sepsentence (char s[])
{
	int i = 0;
	while (s[i] != '\0')
	{
		while (Symbol(s[i]) == 1 && s[i] != '\0')
		    i++;
		int si = i;
		while (Symbol(s[i]) == 0 && s[i] != '\0')
		    i++;
		int ei = i-1;
		for (int j = si;j <= ei;j++)
		cout<<s[j];
		cout<<" ";
   }
}

int main()
{
	int i = 0;
	char s[100];
	cout<<"Input:";
	gets(s);
	cout<<"Output:";
	Reverse(s);
	Sepsentence(s);
	return 0;
}




