#include <stdio.h>
#include <string.h>
int main()
{
    char in[100] = "   I   like ,,,...   C++  programming!";
    char out[100] = "";
    int j = 0;
    int i = 0;
    int readMode = 0;
    for (i = 0; in[i] != '\0'; i++)
    {
        if (in[i] == ' ' || in[i] == ',' || in[i] == '.' || in[i] == '?' || in[i] == '!')
        {
            if (readMode)
            {
                out[j] = '\0';
                puts(out);
            }
            readMode = 0;
        }
        else
        {
            if (readMode)
            {
                out[j] = in[i];
                j++;
            }
            else
            {
                j = 0;
                memset(out, 0, sizeof(out));
                out[j] = in[i];
                j++;
                readMode = 1;
            }
        }
    }
    if (readMode)
    {
        out[j] = in[i];
        puts(out);
    }
    return 0;
}
