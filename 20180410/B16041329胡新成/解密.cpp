#include<iostream>
#include<cstring>
using namespace std;
int main(){
    char code[100]="";
    gets(code);
    int len=strlen(code);
    for(int i=0;i<len;i++){
        if ('A' <= code[i] && code[i] <= 'Z')
            code[i] = (code[i] + 3 - 'A') % 26 + 'a';
        else if ('a' <= code[i] && code[i] <= 'z')
            code[i] = (code[i] + 3 - 'a') % 26 + 'A';
    }
    char temp;
    for(int i=0;i<=len/2;i++){
        for(int j=len-1;j>=len/2;j--){
            if(i+j==len-1){
                temp=code[i];
                code[i]=code[j];
                code[j]=temp;
            }
        }
    }
    puts(code);
    return 0;
}
