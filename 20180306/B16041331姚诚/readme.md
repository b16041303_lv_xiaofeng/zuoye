#Symmetric Matrices
sm.c 为测试程序源码

```

$ ./sm
    1    2    3    4
    
    2    5    6    7
    
    3    6    8    9
    
    4    7    9   10
    
```

也可以直接引用 sym.h头文件

```
#include<iostream>
#include<iomanip>
#include "sym.h"
using namespace std;
int main(){
	int **m;
	m=sm(4); // int ** sm(int n) return n*n Symmetric Matrice
	//show m
	for(int i =0;i<4;i++){
		for (int j=0;j<4;j++){
			cout<<setw(5)<<m[i][j];
		}
		cout<<endl;
	}
}
```