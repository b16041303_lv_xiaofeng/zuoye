#include <stdio.h>
int main()
{
    int i;
    char in[100] = "";
    gets(in);
    for (i = 0; in[i] != '\0'; i++)
    {
        if ('a' <= in[i] && in[i] <= 'z')
            in[i] = (in[i] + 2 - 'a') % 26 + 'a';
        else if ('A' <= in[i] && in[i] <= 'Z')
            in[i] = (in[i] + 2 - 'A') % 26 + 'A';
        else if ('0' <= in[i] && in[i] <= '9')
            in[i] = (in[i] + 2 - '0') % 10 + '0';
    }
    puts(in);
    return 0;
}
