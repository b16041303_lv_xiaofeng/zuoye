一、// 利用C语言实现：输入多个字符（小写字母），进行如下变换后输出

/*
输入：abcxyz
输出：cdezab
*/
提示：使用getchar()函数和putchar()函数

二、利用C语言中的 fgetc()  fputc() 实现文本文件复制
从
main ( int argc, char **argv )从argv中获取srcfile和destfile
文件操作的三大要素：打开文件、操作文件和关闭文件， fopen, fputc,fgetc,feof和fclose